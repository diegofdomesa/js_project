/* var englishGreeting = 'Hello World'
englishGreeting = 'Hi World'
console.log(typeof(englishGreeting))

console.log(englishGreeting)
englishGreeting = "1"

console.log(typeof(englishGreeting))
console.log(englishGreeting)

englishGreeting = true
console.log(typeof(englishGreeting))
console.log(englishGreeting) 
 */
let a='1,',b=1.6,c=true, d = [ {a:1,b:2},{a:4,b:3}] , e = {f:3}
//floating point

console.log(typeof(a))
console.log(typeof(b))
console.log(typeof(c))
console.log(typeof(d))

console.log(d[0].a)
console.log(d[0]['a'])

console.log(e.f)
console.log(e['f'])

let array = [ {a:1,b:2}, 1, 'c']
console.log(typeof(array))

let stringArray = ['1','2',3]
console.log(typeof(stringArray[0]))
console.log(typeof(stringArray[2]))

console.log(Math.abs('a'))