function printMessage() {
    console.log('Hola Mundo Cruel')
}

//printMessage()

/**
 * 
 * @param {integer} a 
 * @param {integer} b 
 */
function add(a,b) {
    
    console.log(a+b)
}
let b
/*add(1,2)
add(3,5)
add(4,8)
add(4,b)*/


function addInfinite() {
    for (let i=0;i<arguments.length;i++){
        console.log(arguments[i])
    }
}

/*addInfinite(1)
addInfinite(1,2,3,4,5,6,7,8,9)*/

function addReturn(a,b) { 
    return a+b
}
let c = addReturn (addReturn(1,2) , addReturn(3,4)+addReturn(5,6))
//console.log( c )

//let addFunctionVariable = (a,b)=>{console.log(a)} //arrow functions
//console.log(addFunctionVariable(1,2))

//let addFunctionVariable2 = function(a,b){console.log(a)}

//Recursive Function

//Fibonacci Serie 
// 0 1 1 2 3 5 8 13 21 34


function fibo (a){
    if (a === 0) {
        return 0
    }
    if (a === 1) {
        return 1
    }
    return (fibo(a-2)+fibo(a-1))   
}

console.log(fibo(8))



