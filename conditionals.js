//ternary operator
/* let equal = true
let a = 1
let b = 1
equal = a==b ? console.log("VERDADERO") : { a : "FALSO"} 
console.log(equal)
console.log(typeof(equal)) */

//If
let a = "b"


console.log(a)
if ( a === 1) {  
    console.log("Dentro del if")
}
else if( a === 2) {
    console.log("Dentro del if 2")
}
else if( a === 3) {
    console.log("Dentro del if 3")
}
else {
    console.log("Dentro del else")
}
console.log("Fuera del if")

switch(a){
    case "a":
        console.log("Dentro del case 1")
        break
    case "b":
        console.log("Dentro del case 2") 
        break
    case "c":
        console.log("Dentro del case 3") 
        break       
    default:
        console.log("Dentro del default")    
}         


/**
 * alba - error
 * sebas - NPI  
 * mayda -error
 * ana - NPI
 */

// incrementals and decrementals -- counters

/*let c = 1
console.log(c) //1
*/
/* console.log(c++) //que imprime aca
console.log(c)
console.log(c--) //que imprime aca
console.log(c)
 */

/* console.log(++c) //que imprime aca
console.log(c)
console.log(--c) //que imprime aca
console.log(c)
 */

// dates
/* 
const date = new Date()
console.log(date)
console.log(date.getDate()) */