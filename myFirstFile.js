console.log('Hola Mundo');

console.log("Hello World")
console.log(`Con tilde invertida`) //string literal

const MESSAGE_FOR_EXAMPLE = 1

console.log('constante:'+typeof(MESSAGE_FOR_EXAMPLE))


let englishGreeting
englishGreeting = 'Hi World'


console.log(typeof(englishGreeting))

console.log('Number Max value:'+Number.MAX_VALUE)

console.log(englishGreeting)
englishGreeting = 1n

console.log(typeof(englishGreeting))
console.log(englishGreeting)

englishGreeting = true
console.log(typeof(englishGreeting))
console.log(englishGreeting)


let undefinedVar
console.log(typeof(undefinedVar))

let customer = {
 'name': 'pepito perez',
 'phone': 3222222,
 'age': 97
}
console.log(typeof(customer))
console.log(customer)
console.log(customer.name)

let customerList = [ {
    'name': 'pepito perez',
    'phone': 3222222,
    'age': 97
   
   },
   {
    'name': 'pepito rodriguez',
    'phone': 3222222,
    'age': 123
   
   },
   {
    'name': 'maximiliano rodriguez',
    'phone': 3222222,
    'age': 7
   
   }
]

console.log(customerList)
console.log(customerList[1])
console.log(customerList[1].phone)




