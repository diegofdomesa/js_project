let array = [{"a":0},{"a":0},{"a":0},{"a":0}]
console.log('for in')

for ( let a in array){
    console.log(typeof(array[a]))
    console.log(a)
    console.log(array[a])
}

console.log('for of')
for ( let a of array){
    console.log(typeof(a))
    console.log(a)    
}

console.log('for 1')
for (let i = 0; i<array.length; i++){
    console.log(array[i])
}

console.log('for 2')
for (let j = array.length-1; j > -1; j--){
    console.log(array[j])
}
console.log('While')
let k= 0

while ( k<array.length){
    console.log(array[k])
    k++;
}
console.log('do/while')

let l = 0
do{
    console.log(array[l])
    l++;
}while(l<array.length)